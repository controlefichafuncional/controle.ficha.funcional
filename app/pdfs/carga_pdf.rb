#encoding:utf-8
class CargaPdf < Prawn::Document
    def page_layout
        options= {
            page_size: "A4",
            page_layout: :portrait,
            margin: [10, 17],
            top_margin: 50
        }
    end

    def initialize(data_to_report)
        super(self.page_layout)
        @dados = data_to_report
        @header_columns = [
         "Matricula Funcional", 
         "Nome do Servidor",
         "CPF",
         "Função",
         "Lotação"
        ]
        imprimir_carga_em_pdf
    end

    def banner
      logopath =  "#{Rails.root}/app/assets/images/brasao_rgb.png"
      image logopath, :width => 60, :height => 60, at: [0, 820]
      draw_text "PREFEITURA MUNICIPAL DE CAMPOS DOS GOYTACAZES",at: [70, 800], size: 9, style: :bold,horizontal_padding: 30
      draw_text "SECRETARIA MUNICIPAL DE ADMINISTRAÇÃO E GESTÃO DE PESSOAS",at: [70, 785], size: 9, style: :bold,horizontal_padding: 30
      draw_text "SISTEMA DE CONTROLE DE FICHAS FUNCIONAIS",at: [70, 770], size: 9, style: :bold,horizontal_padding: 30
    end

    def header
        self.banner
        move_down 30
        time = Time.now
        data_do_dia = @dados.created_at.strftime("%d/%m/%Y")
        hora_do_dia = @dados.created_at.strftime("%H:%M:%S")
        responsavel = @dados.user_1.nome
        setor_de_origem = @dados.setor_origem.nome
        setor_de_destino = @dados.setor_destino.nome

        draw_text "ESPELHO DE CARGAS ENTRE SETORES - SECRETARIA MUNICIPAL DE ADMINISTRAÇÃO E GESTÃO DE PESSOAS", at: [0, 700], size: 9,horizontal_padding: 10
        draw_text "Data de criação em: #{data_do_dia}",  at: [0, 670], size: 10
        draw_text "Hora da criação: #{hora_do_dia}",     at: [210, 670], size: 10
        draw_text "Usuário responsável: #{responsavel}", at: [350, 670], size: 10
        draw_text "Setor de Origem: #{setor_de_origem}", at: [0, 650], size: 10
        draw_text "Setor de Destino: #{setor_de_destino}", at: [0, 630], size: 10
        draw_text "Identificação da Carga -  #{@dados.id}", at: [0, 610], size: 10
        draw_text "Total de documentos -  #{@dados.line_items.count}", at: [450, 610], size: 10
        move_down 30
    end

    def signature_line
      # stroke do
      #   horizontal_line 0, 0, :at => [130,9], at: [200,200]
      # end
      draw_text "_______________________________", :at => [-125,10], at: [180,200]
      draw_text "Assinatura do responsável",  :at => [230,0.5], at: [210,180]
    end

    def imprimir_carga_em_pdf
        self.header
        move_down 100

        draw_table(table_itens)
        move_down 20
        
        move_down 20
        signature_line
    end

    def draw_table(collection)
      itens = Array.new(collection) {collection}
      move_down 20
      table itens do
        row(0).style(
          :background_color => 'FFFFFF', 
          :size => 9, 
          :align => :center, 
          :font_style => :bold, 
          :border_width => 0.5,
          :height => 18
        )

        rows(1).width = 45

        

        # row_colors = ["C1FFC1","FFFFFF"]
        column(0).style(:row_color => 'ffffff', :height => 18)
        cells.style(:border_width => 0.5, :width => 112, :align => :center ,:size=>8)
       
        # cells[0,0].background_color = 'ffffff'
        header = true
      end
    end

    def table_itens
          @dados.line_items.map { |item|  
            [
              item.ficha.matricula,
              item.ficha.nome,
              item.ficha.cpf,
              item.ficha.nome_funcao,
              item.ficha.nome_lotacao,
            ]
          }.insert(0, @header_columns)
    end
end