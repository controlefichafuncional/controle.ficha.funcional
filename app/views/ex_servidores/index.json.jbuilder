json.array!(@ex_servidores) do |ex_servidor|
  json.extract! ex_servidor, :id, :nova_matricula, :matricula_antiga, :nome, :cpf, :data_de_admissao, :data_de_desligamento, :data_de_aposentadoria, :vinculo, :nome_função
  json.url ex_servidor_url(ex_servidor, format: :json)
end
