json.array!(@order_cargas) do |order_carga|
  json.extract! order_carga, :id, :gerada_pelo_usuario_id, :recebida_pelo_usuario_id, :setor_de_origem_id, :setor_de_destino_id
  json.url order_carga_url(order_carga, format: :json)
end
