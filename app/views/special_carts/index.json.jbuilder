json.array!(@special_carts) do |special_cart|
  json.extract! special_cart, :id
  json.url special_cart_url(special_cart, format: :json)
end
