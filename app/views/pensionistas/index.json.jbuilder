json.array!(@pensionistas) do |pensionista|
  json.extract! pensionista, :id, :matricula, :cpf, :nome, :situacao, :data_admissao
  json.url pensionista_url(pensionista, format: :json)
end
