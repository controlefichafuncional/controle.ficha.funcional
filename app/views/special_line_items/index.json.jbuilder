json.array!(@special_line_items) do |special_line_item|
  json.extract! special_line_item, :id, :special_cart_id, :exservidor_id, :order_carga_id
  json.url special_line_item_url(special_line_item, format: :json)
end
