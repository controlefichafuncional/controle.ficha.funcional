class ExServidoresController < ApplicationController
  before_action :set_ex_servidor, only: [:show, :edit, :update, :destroy]

  # GET /ex_servidores
  # GET /ex_servidores.json
  def index
    @ex_servidores = ExServidor.all
  end

  # GET /ex_servidores/1
  # GET /ex_servidores/1.json
  def show
  end

  # GET /ex_servidores/new
  def new
    @ex_servidor = ExServidor.new
  end

  # GET /ex_servidores/1/edit
  def edit
  end

  # POST /ex_servidores
  # POST /ex_servidores.json
  def create
    @ex_servidor = ExServidor.new(ex_servidor_params)

    respond_to do |format|
      if @ex_servidor.save
        format.html { redirect_to @ex_servidor, notice: 'Ex servidor was successfully created.' }
        format.json { render action: 'show', status: :created, location: @ex_servidor }
      else
        format.html { render action: 'new' }
        format.json { render json: @ex_servidor.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /ex_servidores/1
  # PATCH/PUT /ex_servidores/1.json
  def update
    respond_to do |format|
      if @ex_servidor.update(ex_servidor_params)
        format.html { redirect_to @ex_servidor, notice: 'Ex servidor was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @ex_servidor.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /ex_servidores/1
  # DELETE /ex_servidores/1.json
  def destroy
    @ex_servidor.destroy
    respond_to do |format|
      format.html { redirect_to ex_servidores_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ex_servidor
      @ex_servidor = ExServidor.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ex_servidor_params
      params.require(:ex_servidor).permit(:nome, :nova_matricula, :cpf, :data_de_desligamento, :vinculo, :nome_funcao, :setor_atual_id, :matricula_antiga)
    end
end
