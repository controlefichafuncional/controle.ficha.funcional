class OrderCargasController < ApplicationController
  include CurrentCart #IMPORTA CURRENT-CART PARA VERIFICA SE EXISTE UM CARRINHO DE FICHAS EM SESSÃO
  include SetStatus   #IMPORTA SET-STATUS PARA INICIALIZAR UMA CLASSE STATUS PARA ENTÃO SETAR O ATRIBUTO DE CLASSE STATUS 
  
  before_action :authenticate_usuario!
  before_action :current_usuario
  before_action :set_cart, only: [:new, :create]

  '''
    A ACTION VIEW INDEX ABAIXO SE DESTINA APENAS A 
    EXIBIÇÃO DAS CARGAS CRIADAS PELO USUARIO CASO ESTE
    FIQUE MUITO TEMPO PARADO NA ACTION SHOW BEM COMO A 
    SESSÃO TENHA EXPIRADO UMA OUTRA FORMA DE ESTE USUARIO
    PODER IMPRIMIR NOVAMENTE O ESPELHO DE CARGA ENTRE SETORES
  '''

  def index
    if current_usuario
        @order_cargas = OrderCarga.where(setor_de_origem_id: current_usuario.setor_id).order('created_at DESC').paginate(:page => params[:page], :per_page=>20)
        if @order_cargas.empty?
          flash[:error] = t('carga.carga_empty')
          redirect_to sisconff_path
        end
    else
        sign_out current_usuario, :bypass => true
        flash[:error] = t('carga.access_deny')
        redirect_to sisconff_path
    end
  end

  def show
    if current_usuario
      @order_carga = OrderCarga.find(params[:id])
    end
  end

  def new
    case @cart
      when @cart.line_items.empty?
          redirect_to control_set_new_carga_path, notice: t('carga.cart_empty')
          return
    end
    @order_carga = OrderCarga.new
    @status = Status.new
  end

  def edit
    @carga = OrderCarga.find(params[:id])
    @carga.update_attributes(:status=> true, :user_2 => current_usuario, :updated_at => Time.now)
    
    if @carga.fichas.exists?
      @carga.fichas.update_attributes(:setor_atual_id=> @carga.setor_de_destino.id)
      flash[:notice] = t('carga.received')
      redirect_to sisconff_path
    elsif @carga.ex_servidores.exists?
      @carga.ex_servidores.update_attributes(:setor_atual_id=> @carga.setor_de_destino.id)
      flash[:notice] = t('carga.received')
      redirect_to sisconff_path
    end
  end

  def create
    @status = Status.new
    @order_carga = OrderCarga.new(order_carga_params)
    @order_carga.created_at = Time.now
    
    if @cart
      @order_carga.add_line_items_from_cart(@cart)  
      @order_carga.status = @status.set_status_off
    end

    respond_to do |format|
      if @order_carga.save
        if @cart
          Cart.destroy(session[:cart_id])
          session[:cart_id] = nil
        end
        format.html { redirect_to order_cargas_id_path(@order_carga.id), notice: t('carga.successifully_created') }
        format.json { render action: 'index', status: :created, location: @order_carga }
      else
        format.html { render action: 'new' }
        format.json { render json: @order_carga.errors,
          status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @order_carga = OrderCarga.find(params[:id]).destroy
    flash[:notice] = t("carga.successifully_destroyed")
    redirect_to sisconff_path
  end

  private

  def order_carga_params
    params.require(:order_carga).permit(:gerada_pelo_usuario_id, :recebida_pelo_usuario_id, :setor_de_origem_id, :setor_de_destino_id, :status)
  end
end
