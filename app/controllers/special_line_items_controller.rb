#-*- coding:utf-8 -*-

# def remove_zero_a_esquerda(termo)
#   return termo.gsub( /^0 {n} + /, '').to_i.to_s if termo.is_a? String
# end

class SpecialLineItemsController < ApplicationController
  include CurrentSpecialCart

  before_action :authenticate_usuario!
  before_action :current_usuario
  before_action :set_special_cart, only: [:create]
  before_action :set_special_line_item, only: [:show, :edit, :update, :destroy]

  # GET /special_line_items
  # GET /special_line_items.json
  def index
    @special_line_items = SpecialLineItem.all
  end

  # GET /special_line_items/1
  # GET /special_line_items/1.json
  def show
  end

  # GET /special_line_items/new
  def new
    @special_line_item = SpecialLineItem.new
  end

  # GET /special_line_items/1/edit
  def edit
  end

  # POST /special_line_items
  # POST /special_line_items.json
  def create
    if @ex_servidor = ExServidor.where(nova_matricula: remove_zero_on_the_left(params[:search_string])).first

      # binding.pry
      if !@ex_servidor.pendente? and @ex_servidor.setor_atual_id == current_usuario.setor.id
        @special_line_item = @special_cart.add_matricula(@ex_servidor)
        if @special_cart.special_line_items.find_by(ex_servidor_id: @ex_servidor.id)
          flash[:info] = t("line_item.item_already_added", ficha_numero: @ex_servidor.nova_matricula)
          redirect_to control_new_ex_serv_cargas_path
        else
          respond_to do |format|
            if @special_line_item.save
              format.html { redirect_to control_new_ex_serv_cargas_url }
              # format.js   { @current_item = @line_item }
              format.json { render action: 'show', status: :created, location: @special_line_item }
            else
                format.html { render action: 'new' }
                # flash[:info] = @cart.message_error
                format.json { render json: @special_line_item.errors, status: :unprocessable_entity }
            end
          end
        end
      else
        flash[:error] = t('line_item.access_deny', matricula: @ex_servidor.nova_matricula, setor_atual: @ex_servidor.setor)
        redirect_to control_new_ex_serv_cargas_path
      end
    else
      flash[:error] = t('line_item.special_item_not_found')
      redirect_to control_new_ex_serv_cargas_path
    end
  end

  # PATCH/PUT /special_line_items/1
  # PATCH/PUT /special_line_items/1.json
  def update
    respond_to do |format|
      if @special_line_item.update(special_line_item_params)
        format.html { redirect_to @special_line_item, notice: 'Special line item was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @special_line_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /special_line_items/1
  # DELETE /special_line_items/1.json
  def destroy
    @special_line_item.destroy
    redirect_to control_new_ex_serv_cargas_path
    flash[:notice] = t('line_item.successfully_destroyed')
  end

private
    # Use callbacks to share common setup or constraints between actions.
    def set_special_line_item
      @special_line_item = SpecialLineItem.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def special_line_item_params
      params.require(:special_line_item).permit(:special_cart_id, :exservidor_id, :order_carga_id)
    end
end
