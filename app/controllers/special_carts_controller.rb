class SpecialCartsController < ApplicationController
  before_action :authenticate_usuario!
  before_action :current_usuario, only: [:create, :update, :destroy]
  before_action :set_special_cart, only: [:show, :edit, :update, :destroy]
  after_action :flash_notice, :only => [:new, :create]
  after_action :flash_info, :only => [:new, :create]

  # GET /special_carts
  # GET /special_carts.json
  def index
    @special_carts = SpecialCart.all
  end

  # GET /special_carts/1
  # GET /special_carts/1.json
  def show
  end

  # GET /special_carts/new
  def new
    @special_cart = SpecialCart.new
  end

  # GET /special_carts/1/edit
  def edit
  end

  # POST /special_carts
  # POST /special_carts.json
  def create
    @special_cart = SpecialCart.new(special_cart_params)

    respond_to do |format|
      if @special_cart.save
        format.html { redirect_to @special_cart, notice: 'Special cart was successfully created.' }
        format.json { render action: 'show', status: :created, location: @special_cart }
      else
        format.html { render action: 'new' }
        format.json { render json: @special_cart.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /special_carts/1
  # PATCH/PUT /special_carts/1.json
  def update
    respond_to do |format|
      if @special_cart.update(special_cart_params)
        format.html { redirect_to @special_cart, notice: 'Special cart was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @special_cart.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /special_carts/1
  # DELETE /special_carts/1.json
  def destroy
    @special_cart.destroy
    respond_to do |format|
      format.html { redirect_to special_carts_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_special_cart
      @special_cart = SpecialCart.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def special_cart_params
      params[:special_cart]
    end

    def invalid_cart
      logger.error "Attempt to access invalid cart #{params[:id]}"
      redirect_to sisconff_url, notice: 'Invalid Action'
    end

    def flash_notice
       if !@cart.flash_notice.blank?
          flash[:notice] = @reservation.flash_notice
       end
   end

   def flash_info 
       if !@cart.flash_info.blank?
          flash[:info] = @cart.flash_info
       end
   end
end
