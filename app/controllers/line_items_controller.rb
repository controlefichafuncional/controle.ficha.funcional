#encoding: utf-8

class LineItemsController < ApplicationController
  include CurrentCart
  before_action :authenticate_usuario!
  before_action :current_usuario
  before_action :set_cart, only: [:create]
  before_action :set_line_item, only: [:show, :edit, :update, :destroy]


  def index
    @line_items = LineItem.all
  end

  def show
  end

  def new
    @line_item = LineItem.new
  end

  def edit
  end

  '''
    AO INTERPRETAR A REQUISIÇÃO FEITA EM CONTROL_CONTROLLER O 
    METODO CREATE ABAIXO FAZ A VERIFICAÇÃO DESTE MATRICULA FUNCIONAL
    VERIFICANDO SE ESTA CONTEM EM ALGUMA ORDEM DE CARGA, CASO ESTA ESTEJA 
    ELE BLOQUEARÁ A INSERÇÃO DESTA.
  '''

  def create
    if @ficha = Ficha.where(matricula: remove_zero_on_the_left(params[:search_string])).first
      # @ficha = Ficha.find_by(matricula: params[:search_string])

      # binding.pry
      if !@ficha.pendente? and @ficha.setor_atual_id == current_usuario.setor.id
        @line_item = @cart.add_matricula(@ficha)
        if @cart.line_items.find_by(ficha_id: @ficha.id)
          flash[:info] = t("line_item.item_already_added", ficha_numero: @ficha.matricula)
          redirect_to control_set_new_carga_path
        else
          respond_to do |format|
            if @line_item.save
              format.html { redirect_to control_set_new_carga_url }
              # format.js   { @current_item = @line_item }
              format.json { render action: 'show',
                status: :created, location: @line_item }
            else
                format.html { render action: 'new' }
                # flash[:info] = @cart.message_error
                format.json { render json: @line_item.errors,
                status: :unprocessable_entity }
            end
          end
        end
      else
        flash[:error] = t('line_item.access_deny', matricula: @ficha.matricula ,setor_atual: @ficha.setor_atual.nome)
        redirect_to control_set_new_carga_path
      end
    else
      flash[:error] = t('line_item.item_not_found')
      redirect_to new_ficha_path
    end
  end

  def update
    respond_to do |format|
      if @line_item.update(line_item_params)
        format.html { redirect_to @line_item, notice: t('line_item.successfully_updated') }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @line_item.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @line_item.destroy
    flash[:notice] = t('line_item.successfully_destroyed')
    redirect_to control_set_new_carga_path
  end

private
    def set_line_item
      @line_item = LineItem.find(params[:id])
    end

    def line_item_params
      params.require(:line_item).permit(:ficha_id, :cart_id, :order_carga_id)
    end
end
