module CurrentCart
  extend ActiveSupport::Concern

  private
    '''
    	O MÉTODO CART ESTÁ DESTINADO AOS SERVIDORES 
    	CONVENCIONAIS LOCALIZADOS NA BASE DA SQLSERVER
    	DA FOLHA DE PGTO.
    '''

    def set_cart 
      @cart = Cart.find(session[:cart_id])
    rescue ActiveRecord::RecordNotFound
      @cart = Cart.create
      session[:cart_id] = @cart.id
    end

    # '''
    # 	O MÉTODO SPECIAL CART ESTÁ DESTINADO 
    # 	AOS EX-SERVIDORES LOCALIZADOS NA BASE
    # 	INTERBASE DA FOLHA DE PGTO
    # '''
    # def set_special_cart 
    #   @sepcial_cart = SpecialCart.find(session[:special_cart_id])
    # rescue ActiveRecord::RecordNotFound
    #   @special_cart = SpecialCart.create
    #   session[:special_cart_id] = @special_cart.id
    # end
end
