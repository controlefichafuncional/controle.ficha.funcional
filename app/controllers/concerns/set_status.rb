=begin
  ESTE MÓDULO É RESPONSÁVEL POR
  INICIALIZAR A VARIAVEL STATUS
  EM MODELOS QUE PRECISAM ATIVAR
  ESTA CLAUSULA DURANTE O CREATE
  
  OU SEJA DUARANTE O
  
  @MY_INSTANCE_VAR.SAVE
=end

module SetStatus
  extend ActiveSupport::Concern

  class Status
    def set_status_on
      @status = true
    end

    def set_status_off
      @status = false
    end
  end
end
