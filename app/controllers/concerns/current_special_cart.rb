module CurrentSpecialCart
  extend ActiveSupport::Concern

  private
    '''
    	O MÉTODO SPECIAL CART ESTÁ DESTINADO 
    	AOS EX-SERVIDORES LOCALIZADOS NA BASE
    	INTERBASE DA FOLHA DE PGTO
    '''
    def set_special_cart 
      @special_cart = SpecialCart.find(session[:special_cart_id])
    rescue ActiveRecord::RecordNotFound
      @special_cart = SpecialCart.create
      session[:special_cart_id] = @special_cart.id
    end
end
