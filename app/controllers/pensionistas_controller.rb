class PensionistasController < ApplicationController
  before_action :set_pensionista, only: [:show, :edit, :update, :destroy]

  # GET /pensionistas
  # GET /pensionistas.json
  def index
    @pensionistas = Pensionista.all
  end

  # GET /pensionistas/1
  # GET /pensionistas/1.json
  def show
  end

  # GET /pensionistas/new
  def new
    @pensionista = Pensionista.new
  end

  # GET /pensionistas/1/edit
  def edit
  end

  # POST /pensionistas
  # POST /pensionistas.json
  def create
    @pensionista = Pensionista.new(pensionista_params)

    respond_to do |format|
      if @pensionista.save
        format.html { redirect_to @pensionista, notice: 'Pensionista was successfully created.' }
        format.json { render action: 'show', status: :created, location: @pensionista }
      else
        format.html { render action: 'new' }
        format.json { render json: @pensionista.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /pensionistas/1
  # PATCH/PUT /pensionistas/1.json
  def update
    respond_to do |format|
      if @pensionista.update(pensionista_params)
        format.html { redirect_to @pensionista, notice: 'Pensionista was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @pensionista.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pensionistas/1
  # DELETE /pensionistas/1.json
  def destroy
    @pensionista.destroy
    respond_to do |format|
      format.html { redirect_to pensionistas_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pensionista
      @pensionista = Pensionista.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def pensionista_params
      params.require(:pensionista).permit(:matricula, :cpf, :nome, :situacao, :data_admissao, :nome_vinculo)
    end
end
