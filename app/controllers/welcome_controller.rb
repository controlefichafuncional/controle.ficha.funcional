class WelcomeController < ApplicationController
  before_action :authenticate_usuario!, only: [:sisconff_screen]
  before_action :current_usuario, only: [:sisconff_screen]
  def index
  	redirect_to new_usuario_session_url
  end

  def sisconff_screen
  	'''
  		ESTE SISCONFF É PARA CARREGAR A LOGO
  		DO SISTEMA PARA TODOS QUE UTILIZAREM
  		O SISTEMA
  	'''

    if not current_usuario.cargas_pendentes.empty?
      flash[:info] = "ATENÇÂO - Há Cargas pendentes de recebimento para seu setor!"
      redirect_to control_get_order_carga_url
    else
    end
  end

  def help
  	'''
  		ESTE HELP É PARA AUXILIO DOS USUÁRIOS
  		QUE NÃO CONSEGUEM UTILIZAR O SISTEMA
  	'''
  end
end
