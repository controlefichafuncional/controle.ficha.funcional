class ControlController < ApplicationController
	include CurrentCart        # => IMPORTA CURRENT-CART PARA VERIFICA SE EXISTE UM CARRINHO DE FICHAS EM SESSÃO
  include CurrentSpecialCart # => IMPORTA CURRENT-CART PARA VERIFICA SE EXISTE UM CARRINHO DE FICHAS EM SESSÃO

  before_action :set_cart
  before_action :set_special_cart, :only => [:new_ex_serv_frm]
	
  before_action :authenticate_usuario!
  before_action :current_usuario, :only => [:index, :get_order_carga]

=begin
  
  O INDEX DESTE CONTROLADOR
  QUANDO O USUARIO SE LOGA NO SISTEMA
  ABRE UM MINI FORM DE BUSCA.

  ESTE MESMO FORM DE BUSCA EXECUTA UMA BUSCA
  INTERNA NO MODELO DE FICHAS (MATRICULAS FUNCIONAIS)
  AO EFETUAR A REQUISIÇÃO ESTE FORM ENVIA PARA
  LINE_ITEMS_CONTROLLER ... 

  <CONTINUE LENDO EM LINE_ITEMS_CONTROLLER#CREATE>
  
=end

	def index
    LineItem.delete_all("ficha_id not in (select id from fichas)")
    if params[:set_locale]
        redirect_to control_set_new_carga_url(locale: params[:set_locale])
    end
	end

=begin
  O METODO GET_ORDER_CARGA ESTÁ RESPONSAVEL POR COLETAR TODAS AS CARGAS PENDENTES DE UM USUARIO 
  DE DETERMINADO SETOR. 

  EX:
    @usuario.find(6).cargas_pendentes

    onde cargas_pendentes é um metodo criado no modelo de usuario

    #app/models/usuario
    def cargas_pendentes
      setor.cargas_pendentes 
    end

    O metodo acima vai retornar um metodo descrito no modelo de setor em conjunto com order_cargas cuja linha é igual a: scope :pendentes, -> { where(status: false) }

    Logo este trecho vai retornar todas as cargas que ainda não foram recebidas para o setor do usuario atualmente logado no sistema
=end

  def get_order_carga
    @cargas_pendentes = current_usuario.cargas_pendentes.paginate(page: params[:page], per_page: 10)

    if @cargas_pendentes.empty?
       redirect_to sisconff_url
       flash[:error]= t("control.access_deny_not_found")
       # return
    end
  end

=begin
  O METODO RECIVE_ALL LOGO ABAIXO ESTA SENDO UTILIZADO PARA RECEBER TODAS AS CARGAS
  ENCONTRADAS NO SETOR DESTINO GERADO NA HORA DA CRIAÇÃO DA CARGA. 
=end

  def receive_all
    @cargas = OrderCarga.where(:id => params[:order_cargas_ids])
    @cargas.update_all(
      :status => true, 
      :recebida_pelo_usuario_id => current_usuario.id, :updated_at => Time.now)

    if @cargas.empty?
      flash[:error] = "Opção inválida! Favor selecionar cargas para receber."
      redirect_to root_path
    else
      @cargas.each do |carga|
        carga.fichas.update_all(:setor_atual_id => carga.setor_destino.id)
        carga.ex_servidores.update_all(:setor_atual_id => carga.setor_destino.id)
      end
    end
    
    redirect_to sisconff_path, :notice => t('control.all_received', :appraiser=> current_usuario.nome)
  end

=begin
  OS METODOS VIEW_MATRICULAS E VIEW_EXSERVIDOR_MATRICULAS
  SERVEM PARA VER AS MATRICULAS CONTIDAS EM DETERMINADA CARGA
=end
  
  def view_matriculas
    @carga = OrderCarga.find(params[:carga_id])
  end

  def view_exservidor_matriculas
    @carga = OrderCarga.find(params[:special_carga_id])
  end

=begin
  O MÉTODO ABAIXO TRADUZ A RESPONSABILIDADE DE ABRIR O FORMULÁRIO
  DE INCERÇÃO DE CARGAS PARA EX-SERVIDOR.
=end

  def new_ex_serv_frm
    if params[:set_locale]
        redirect_to control_new_ex_serv_cargas_url(locale: params[:set_locale])
    end
  end
end
