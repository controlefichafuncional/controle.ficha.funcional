#-*- conding:utf-8 -*-
class SpecialOrderCargasController < ApplicationController
	include CurrentSpecialCart #IMPORTA CURRENT-SPECIAL-CART PARA VERIFICA SE EXISTE UM CARRINHO DE EXSERVIDORES EM SESSÃO
	include SetStatus   #IMPORTA SET-STATUS PARA INICIALIZAR UMA CLASSE STATUS PARA ENTÃO SETAR O ATRIBUTO DE CLASSE STATUS 
	before_action :authenticate_usuario!
	before_action :current_usuario
	before_action :set_special_cart, only: [:new, :create]

	def new
		@special_order_carga = OrderCarga.new

  	case @special_cart
      when @special_cart.special_line_items.empty?
          redirect_to control_new_ex_serv_cargas_path, notice: t('carga.cart_empty')
          return
    end
	end

	def create
		@status = Status.new
    @special_order_carga = OrderCarga.new(order_carga_params)
    @special_order_carga.created_at = Time.now
  
  	@special_order_carga.add_special_line_items_from_special_cart(@special_cart)  
    @special_order_carga.status = @status.set_status_off
  
    respond_to do |format|
      if @special_order_carga.save
        SpecialCart.destroy(session[:special_cart_id])
        session[:special_cart_id] = nil
        
        format.html { redirect_to exservidor_cargas_id_path(@special_order_carga.id), notice: t('carga.successifully_created') }
        format.json { render action: 'index', status: :created, location: @special_order_carga }
      else
        format.html { render action: 'new' }
        format.json { render json: @special_order_carga.errors,
          status: :unprocessable_entity }
      end
    end
	end

	def show
    if current_usuario
      @special_order_carga = OrderCarga.find(params[:id])
      unless @special_order_carga.user_1 == current_usuario
          flash[:error] = t('carga.access_deny')
          redirect_to sisconff_path
      end
    end
  end

	def destroy
    @special_order_carga = OrderCarga.find(params[:id]).destroy
    flash[:notice] = t("carga.successifully_destroyed")
    redirect_to sisconff_path
	end
private
  def order_carga_params
    params.require(:order_carga).permit(:gerada_pelo_usuario_id, :recebida_pelo_usuario_id, :setor_de_origem_id, :setor_de_destino_id, :status)
  end
end
