#encoding:utf-8

class PdfReportsController < ApplicationController
	before_action :authenticate_usuario!
	before_action :current_usuario

	def imprimir_carga
		@order_carga = OrderCarga.find(params[:id])
  	respond_to do |format|
      format.html
      format.pdf { 
        pdf = CargaPdf.new(@order_carga)
        send_data pdf.render, 
          filename: "etiqueta_#{@order_carga.created_at.strftime("%d/%m/%Y")}.pdf",
          type: "application/pdf", disposition: "inline"
      }
      format.json { render json: @order_carga }
		end
	end

  def imprimir_exservidor_carga
    @special_order_carga = OrderCarga.find(params[:id])
    respond_to do |format|
      format.html
      format.pdf { 
        pdf = SpecialCargaPdf.new(@special_order_carga)
        send_data pdf.render, 
          filename: "etiqueta_#{@special_order_carga.created_at.strftime("%d/%m/%Y")}.pdf",
          type: "application/pdf", disposition: "inline"
      }
      format.json { render json: @special_order_carga }
    end
  end
end
