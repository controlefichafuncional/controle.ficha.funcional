class UsuariosController < ApplicationController
	before_action :authenticate_usuario!
	before_action :current_usuario
	
  def index
  end

  def edit
  	@usuario = Usuario.find(current_usuario.id)
  end

  def update_password
  	@usuario = Usuario.find(current_usuario.id)
  	if @usuario.update_attributes(password_params)
        sign_in @usuario, :bypass => true
        flash[:notice] = "A sua senha foi alterada com sucesso. A partir de agora deverá usar sua nova senha."
        redirect_to sisconff_path
    end 
  end


private
	def password_params
		params.require(:usuario).permit(:password, :password_confirmation)
	end

	def user_params
		params.require(:usuario).permit(:nome, :cpf, :matricula,:email, :password, :password_confirmation, :remember_me, :setor, :admin)
	end  
end