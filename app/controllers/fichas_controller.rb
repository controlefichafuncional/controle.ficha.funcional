#encoding:utf-8
class FichasController < ApplicationController
  before_action :authenticate_usuario!
  before_action :current_usuario

  before_action :set_ficha_funcional, only: [:edit, :update, :destroy]

  def index
    @fichas = Ficha.where(user_id: current_usuario.id)
    if @fichas.empty?
      flash[:info] = t("fichas.not_found")
      redirect_to control_path
    end
  end

  def new
      @ficha = Ficha.new
  end

  def edit
  	'''
  		O Objeto está sendo retornando pelo before_action
  		muito mais simplificado reduzindo a replica de 
  		linhas de codigo
  	'''
  end

   def create
    @ficha = Ficha.new(ficha_params.merge("setor_atual_id" => current_usuario.setor_id))
    respond_to do |format|
      if @ficha.save
        format.html { redirect_to control_set_new_carga_path, notice: t("fichas.successifully_created")}
        format.json { head :no_content }
      else
        format.html { render action: 'new' }
        format.json { render json: @cart.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @ficha.update(ficha_params)
        format.html { redirect_to _fichas_path, notice: t("fichas.successifully_updated") }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @ficha.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
  	@ficha.destroy
  	redirect_to standard_path, notice: t("fichas.successifully_destroyed")
  end

private
  def set_ficha_funcional
    @ficha = Ficha.find(params[:id])
  end

  def ficha_params
    params.require(:ficha).permit(:matricula, :nome, :cpf, :nome_funcao, :nome_lotacao, :nome_local_trabalho, :data_admissao, :situacao, :nome_vinculo, :user_id, :pensionista, :setor_atual_id)
  end

  def finish_create_ficha
    flash[:notice] = "Ficha #{@ficha.matricula} inserida com sucesso."
    sisconff_path
  end
end
