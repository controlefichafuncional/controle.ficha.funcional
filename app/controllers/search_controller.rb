#-*- coding:utf-8 -*-
class SearchController < ApplicationController
    before_action :authenticate_usuario!
    before_action :current_usuario

    respond_to :html, :xml

    def index
      session[params[:controller]]= request.fullpath
    end

    '''
      OS METODOS ABAIXO track e locate_matricula
      SÃO RESPONSAVEIS PELO MODULO DE PESQUISAR
      LOCALIZAÇÃO DA FICHA FUNCIONAL.
    '''

    def track
      @title = "Localizar"
      respond_with :html
    end

    def track_by_name
      @title = "Localizar"
      respond_with :html
    end

    def localizar
      @ficha = Ficha.find_by(matricula: remove_zero_on_the_left(params[:search_string]))
      @exservidor = ExServidor.find_by(nova_matricula: remove_zero_on_the_left(params[:search_string]))
      
      if @ficha.nil? and @exservidor.nil?
          flash[:info] = t('search.not_found')
           redirect_to find_matricula_path          
      elsif @ficha and @exservidor
         redirect_to find_paradise_city_path(@ficha.id, @exservidor.id) 
      elsif @ficha
         redirect_to(find_ficha_omega_path(@ficha.id))
      else
         redirect_to find_exservidor_omega_path(@exservidor.id)
      end
    end

    # => START::VIEWS::LOCALIZADORAS_DE_FICHAS
      def locate_ficha
        @ficha = Ficha.find_by(id: params[:ficha_id])
        @setor, @hora_da_criacao, @hora_de_recebimento = [],[],[]
        @fichas = @ficha.order_cargas.paginate(:per_page => 5, :page => params[:page])
        @pensionista = Pensionista.where(matricula: @ficha.matricula)
        session['search'] ||= request.referer
      end

      def locate_exservidor
        @exservidor = ExServidor.find(params[:exservidor_id])
        @setor, @hora_da_criacao, @hora_de_recebimento = [],[],[]
        @exservidores = @exservidor.order_cargas.paginate(:per_page => 5, :page => params[:page])
        session['search'] ||= request.referer
      end

      def locate_both
          @ficha = Ficha.find(params[:ficha_id])
          @exservidor = ExServidor.find(params[:exservidor_id])
          session['search'] ||= request.referer
      end
    # => END::VIEWS::LOCALIZADORAS_DE_FICHAS


    # => O METODO A SEGUIR É O MESMO DO DE CIMA SÓ QUE BUSCA POR NOME
      def localizar_por_nome
        @fichas= Ficha.where('match (nome) against (?)', params[:search_name]).paginate(:per_page => 10, :page => params[:page])
        @exservidores = ExServidor.where('match (nome) against (?)', params[:search_name]).paginate(:per_page => 10, :page => params[:page])
        flash[:info]= t('search.server_name_founded') if @fichas.present? and @exservidores.present?
        session['search'] ||= request.referer
      end
    # => 

    def track_by_sector
      @setores = Setor.all
      respond_with :html
    end

    def locate_fichas_by_sector
      @setor = Setor.find(params[:id])
      @fichas = @setor.fichas.paginate(:per_page => 10, :page => params[:page])
      @ex_servidores = @setor.ex_servidores.paginate(:per_page => 10, :page => params[:page])
      session['search'] ||= request.referer
    end
end
        
