#encoding:utf-8
class SetoresController < ApplicationController
	before_action :authenticate_usuario!
	before_action :current_usuario
	
	private
		def set_setor
			@setor = Setor.find_by(params[:id])
		end

		def setor_params
			params.require(:setor).permit(:nome)
		end
end
