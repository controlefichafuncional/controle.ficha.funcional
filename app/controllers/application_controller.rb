# Prevent CSRF attacks by raising an exception.
# For APIs, you may want to use :null_session instead.
class ApplicationController < ActionController::Base
  	
	protect_from_forgery with: :exception
	before_action :set_i18n_locale_from_params
	

	def registra_url_sessao
		session[params[:controller]]= request.referer
	end

	def remove_zero_on_the_left(termo)
  		return termo.gsub( /^0 {n} + /, '').to_i.to_s if termo.is_a? String
	end
 	
protected
	'''
 		O METODO protected set_i18n_locale_from_params SETA O locale EM QUE A 
 		APLICAÇÃO SE ENCONTRA DEFINIDO LÁ EM APPICATION.RB
 	'''
	def set_i18n_locale_from_params
				if params[:locale]
					if I18n.available_locales.map(&:to_s).include?(params[:locale])
							I18n.locale = params[:locale]
					else
							flash.now[:notice] = "#{params[:locale]} translation not available"
							logger.error flash.now[:notice]
					end
				end
		end

		def default_url_options
			{ locale: I18n.locale }
		end
end
