# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.

jQuery ->
  $('#order_carga_recebida_pelo_usuario_id').parent().hide()
  funcionarios = $('#order_carga_recebida_pelo_usuario_id').html()
  $('#order_carga_setor_de_destino_id').change ->
    setor_de_destino = $('#order_carga_setor_de_destino_id :selected').text()
    escaped_setor_de_destino = setor_de_destino.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
    options = $(funcionarios).filter("optgroup[label='#{escaped_setor_de_destino}']").html()
    if options
      $('#order_carga_recebida_pelo_usuario_id').html(options)
      $('#order_carga_recebida_pelo_usuario_id').parent().show()
    else
      $('#order_carga_recebida_pelo_usuario_id').empty()
      $('#order_carga_recebida_pelo_usuario_id').parent().hide()