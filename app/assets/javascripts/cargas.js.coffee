# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

jQuery ->
  $('#carga_recebida_pelo_usuario_id').parent().hide()
  funcionarios = $('#carga_recebida_pelo_usuario_id').html()
  $('#carga_setor_de_destino_id').change ->
    setor_de_destino = $('#carga_setor_de_destino_id :selected').text()
    escaped_setor_de_destino = setor_de_destino.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
    options = $(funcionarios).filter("optgroup[label='#{escaped_setor_de_destino}']").html()
    if options
      $('#carga_recebida_pelo_usuario_id').html(options)
      $('#carga_recebida_pelo_usuario_id').parent().show()
    else
      $('#carga_recebida_pelo_usuario_id').empty()
      $('#carga_recebida_pelo_usuario_id').parent().hide()

jQuery ->
  $('form').on 'click', '.remove_fields', (event) ->
    $(this).prev('input[type=hidden]').val('1')
    $(this).closest('fieldset').hide()
    event.preventDefault()

  $('form').on 'click', '.add_fields', (event) ->
    time = new Date().getTime()
    regexp = new RegExp($(this).data('id'), 'g')
    $(this).before($(this).data('fields').replace(regexp, time))
    event.preventDefault()