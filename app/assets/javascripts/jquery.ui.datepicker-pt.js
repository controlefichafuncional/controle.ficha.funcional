$(function(){
	$('#datepicker').datepicker({
		inline: true,
		
		
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho',
		'Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		monthNamesShort: ['Jan.','Fev.','Mar','Abr','Mai','Jun', 'Julh.','Ago','Set.','Out.','Nov.','Dez.'],
		dayNames: ['Segunda','Terça','Quarta','Quinta','Sexta','Sabado','Domingo'],
		dayNamesShort: ['Sag.','Ter.','Qua.','Qui.','Sex.','Sab.','Dom.'],


		dateFormat: 'dd/mm/yy',
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		showOn: "button",
		buttonImage: "/images/calendar.png",
		buttonImageOnly: true,
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
	});
});
