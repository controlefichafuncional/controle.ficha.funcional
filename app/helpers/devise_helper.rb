module DeviseHelper
	def devise_error_messages!
		return '' if resource.errors.empy?

		messages = resource.errors.full_messages.map { |map| content_tag(:li, msg) }.join
		html = <<-HTML
		<div class="alert alert-error alert-danger alert-block alert-success"> <button type="button" 
			class="close" data-dismiss="alert">x</button>
			#{messages}
		</div>
		HTML

		html.html_safe
	end
end