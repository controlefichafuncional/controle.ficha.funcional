module ApplicationHelper
	def active_link_to(name = nil, options = nil, html_options = nil, &block)
  	active_class = html_options[:active] || "active"
  	html_options.delete(:active)
  	html_options[:class] = "#{html_options[:class]} #{active_class}" if current_page?(options)
  	link_to(name, options, html_options, &block)
	end

	def error_tag(model, attribute)
		if model.errors.has_key? attribute
			content_tag :div, model.errors[attribute].first, :class => 'error_message'
		end
  end

  def formata_matriculas(item)
  	_hash = []
  	_hash << item
    _hash.join(", ")
	end

	def formata_data(date)
		# formatting date: Aug, 31 2007 - 9:55PM
		date.strftime("%d/%m/%Y às %H:%M")
	end

	def formata_data_admissao(date)
		# formatting date: Aug, 31 2007 - 9:55PM
		date.strftime("%d/%m/%Y")
	end

	def match_string_to_date_format(str)
    	return nil if (str.nil? or str.size < 8)
    	str.match /^(\d{1,2})\/(\d{1,2})\/(\d\d\d\d)/
    	r = ("%02d-%02d-%s" % [str[1], str[2].to_i, str[3]]).to_s
    	r
  	end
	
	def str_lowercase(string)
		return string.downcase
	end

	def to_upcase(string)
		return string.upcase
	end

	def matricula_inserida_manualmente?(flash_type)
		value ? I18n.t('labels._on') : I18n.t('labels._off')
	end

	def recebida_yes_or_no?(value)
		value ? I18n.t('labels._yes_received') : I18n.t('labels._not_received')
	end

	def flash_message
	    messages = ""
	    [:notice, :info, :warning, :error].each {|type|
	      if flash[type]
	        messages += "<div class=\"#{type}\">#{flash[type]}</div>"
	      end
	    }
    	messages.html_safe
  	end

  	def bootstrap_class_messages_for(flash_type)
  		case flash_type
	  		when :notice
	  			"notice"
	  		when :sucess
	  			"success"
	  		when :error
	  			"error"
	  		when :warning
	  			"warning"
			when :info
	  			"info"
	  		# else
	  		# 	flash_type.to_s
  		end
  	end

  	def hidden_div_if(condition, attributes = {}, &block)
    	if condition
      		attributes["style"] = "display: none"
    	end
    	content_tag("div", attributes, &block)
  	end
end
