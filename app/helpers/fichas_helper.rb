module FichasHelper
	def vinculo_servidor_helper
		vinculos_hash = {
			1=> 'estatutario',
			2=>'contratado',
			3=> 'das'
		}
  		vinculos_hash.find_all.collect { |m| [m.last] }
	end

	def situacao_servidor_helper
		situation_hash = {
			1=> 'ativo',
			2=> 'desligado',
			3=> 'aposentado',
			4=> 'pensionista'
		}
  		situation_hash.find_all.collect { |m| [m.last] }
	end
end
