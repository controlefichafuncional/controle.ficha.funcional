#encoding:utf-8

class Usuario < ActiveRecord::Base
  include ActiveModel::Validations
  
  devise 	:database_authenticatable, 
  				:registerable,
         	:recoverable, 
         	:rememberable, 
         	:trackable, 
         	:validatable,
          :timeoutable
         	# :confirmable, 
         	# :lockable, 

  #attr_accessor :email
  belongs_to :setor
  belongs_to :carga

  validates_presence_of :cpf ,:matricula, :setor_id

  validates_format_of :cpf, with: /^[0-9]{3}\.[0-9]{3}\.[0-9]{3}\-[0-9]{2}$/, :multiline => true

  validates_length_of   :nome, 			:maximum => 20
  
  validates_length_of   :matricula, :maximum => 20
 
  validates_presence_of :email, uniqueness: false, allow_blank: true, if: :email_required? 

  validates :nome, presence: true, uniqueness: true, length: { maximum: 50 },allow_blank: false

  def cargas_geradas
    setor.cargas_geradas
  end

  def cargas_pendentes
    setor.cargas_pendentes
  end

  def name_required?
    true;
  end

  def email_required?
    false;
  end

  validate :transformar_nome_em_caixa_alta
private
    def transformar_nome_em_caixa_alta
      self.nome.mb_chars.upcase.to_s if self.nome
    end

  scope :buscar_pelo_nome_do_usuario, -> { order(:matricula) }
  has_paper_trail
end