#encoding:utf-8
class Pensionista < ActiveRecord::Base

	has_paper_trail
	
	validates :nome, presence: true, allow_blank: false
	validate :matricula, presence: true, allow_blank: true, uniqueness: false
	validate :cpf, allow_blank: true, uniqueness: false, presence: true
	validates :situacao, presence: true, allow_blank: false
	validates :nome_vinculo, presence: true

	# ....
		validate :transformar_campos_em_caixa_alta
	# ....	
private
	def transformar_campos_em_caixa_alta
		self.matricula.mb_chars.upcase.to_s if self.matricula
		self.nome.mb_chars.upcase.to_s if self.nome
		self.cpf.mb_chars.upcase.to_s if self.cpf
		self.situacao.mb_chars.upcase.to_s if self.situacao
		self.nome_vinculo.mb_chars.upcase.to_s if self.nome_vinculo
	end
end
