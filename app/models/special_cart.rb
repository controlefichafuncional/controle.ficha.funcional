#-*- coding:utf-8 -*-
class SpecialCart < ActiveRecord::Base
	has_many :special_line_items, dependent: :destroy

	def add_matricula(objeto)
  	current_item = special_line_items.find_by(ex_servidor_id: objeto.id)
  	if current_item 
    		@message_error = "Não pode ser inserido" 
  	else
    		current_item = special_line_items.build(ex_servidor_id: objeto.id)  
  	end
  	current_item
	end
end