class OrderCarga < ActiveRecord::Base
	belongs_to 	:user_1,
				:class_name => "Usuario",
				:foreign_key => "gerada_pelo_usuario_id"
	
	belongs_to 	:user_2,
				:class_name => "Usuario",
				:foreign_key => "recebida_pelo_usuario_id"	

	belongs_to 	:setor_origem,
				:class_name => "Setor",
				:foreign_key => "setor_de_origem_id"

	belongs_to 	:setor_destino,
				:class_name => "Setor",
				:foreign_key => "setor_de_destino_id"

	has_many :usuarios
	has_many :setores
	
	has_many :line_items, dependent: :destroy # => Items para as fichas normais
	has_many :fichas, through: :line_items

	has_many :special_line_items, dependent: :destroy # =>  Items para as fichas especiais - Ex-Servidores
	has_many :ex_servidores, through: :special_line_items

	scope :geradas, 	-> { where(status: true) }
	scope :pendentes, 	-> { where(status: false) }

  	validates_presence_of :setor_de_destino_id

	'''
		O METODO ABAIXO É AQUELE QUEM VAI SER RESPONSAVEL 
		POR ADICIONAR AS FICHAS POSTAS ANTERIORMENTE NO 
		CARRINHO DE MATRICULAS.
	'''
	def add_line_items_from_cart(cart)
		cart.line_items.each do |item|
			item.cart_id = nil
			line_items << item
		end
	end

	def recebida?
		status == true
	end

	def pendente?
		!recebida?
	end

	'''
		O METODO ABAIXO É AQUELE QUEM VAI SER RESPONSAVEL 
		POR ADICIONAR AS FICHAS DE EX-SERVIDORES POSTAS 
		ANTERIORMENTE NO CARRINHO DE MATRICULAS.
	'''
	def add_special_line_items_from_special_cart(special_cart)
		special_cart.special_line_items.each do |item|
			item.special_cart_id = nil
			special_line_items << item
		end
	end

  has_paper_trail
end
