class LineItem < ActiveRecord::Base
  validates_uniqueness_of :ficha_id, :scope => :order_carga_id

  belongs_to :ficha,  class_name: "Ficha",
                      foreign_key: "ficha_id"

  belongs_to :cart,	class_name: "Cart",
                    foreign_key: "cart_id"
                    
  belongs_to :order_carga, 	class_name: "OrderCarga",
                      		foreign_key: "order_carga_id",
                      		dependent: :delete
end
