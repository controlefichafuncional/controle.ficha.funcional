#encoding:utf-8
class Ficha < ActiveRecord::Base
	has_paper_trail
	
	validates_presence_of :nome, uniqueness: false
	validates_presence_of :nome_funcao, uniqueness: false
	validates_presence_of :nome_lotacao, uniqueness: false
	validates_presence_of :data_admissao, uniqueness: false
	validates_presence_of :situacao, uniqueness: false
	validates_presence_of :nome_vinculo, uniqueness: false

	validates :matricula, presence: true, uniqueness: true
	validates :cpf, allow_blank: true, uniqueness: false, presence: false
	validates :nome_local_trabalho, presence: true, uniqueness: false

	validates :nome_vinculo, presence: false, allow_blank: true, uniqueness: false
	validates :situacao, presence: false, allow_blank: true, uniqueness: false
	
	# ....
		validate :transformar_campos_em_caixa_alta
	# ....
	
	
	#validates_uniqueness_of :matricula, data: { confirm: "Favor efetuar a carga normalmente" }

	def esta_em_setor
		order_cargas.geradas
	end

	def pendente?
		order_cargas.where(status: false).any?
	end

	has_many :line_items
	has_many :order_cargas, through: :line_items

	belongs_to 	:setor_atual,
				:class_name => "Setor",
				:foreign_key => "setor_atual_id"

	
	def self.search_by_matricula(search)
  		if search
    		find_by('matricula LIKE ?', "%#{search}%")
  		else
    		scoped
  		end
	end

	def self.search_by_name(search)
  		if search
    		find_by('nome LIKE ?', "%#{search}%")
  		else
    		scoped
  		end
	end


private
	def transformar_campos_em_caixa_alta
		self.nome.mb_chars.upcase.to_s if self.nome
		self.nome_funcao.mb_chars.upcase.to_s if self.nome_funcao
		self.nome_lotacao.mb_chars.upcase.to_s if self.nome_lotacao
		self.situacao.mb_chars.upcase.to_s if self.situacao
		self.nome_vinculo.mb_chars.upcase.to_s if self.nome_vinculo
	end
end
