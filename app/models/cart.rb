#-*- coding:utf-8 -*-
class Cart < ActiveRecord::Base
	has_many :line_items, dependent: :destroy

  def add_matricula(objeto)
    current_item = line_items.find_by(ficha_id: objeto.id)
    if current_item 
      @message_error = "Não pode ser inserido" 
    else
      current_item = line_items.build(ficha_id: objeto.id)  
    end
    current_item
  end

  def total_matriculas
    line_items.to_a.sum { |item| item.total_total_matriculas }
  end
end
