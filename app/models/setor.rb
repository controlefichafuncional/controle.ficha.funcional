#encoding:utf-8
class Setor < ActiveRecord::Base
	has_many :usuarios
	has_many :cargas, class_name: "OrderCarga", :foreign_key => "setor_de_destino_id"
	has_many :fichas, class_name: "Ficha", :foreign_key => "setor_atual_id"
	has_many :ex_servidores, class_name: "ExServidor", :foreign_key => "setor_atual_id"

	def cargas_geradas
		cargas.geradas
	end	

	def cargas_pendentes
		cargas.pendentes
	end
	
	validates_presence_of :nome
	validates_uniqueness_of :nome
	validate :transformar_nome_em_caixa_alta
	
	def name; nome end

	has_paper_trail
	scope :buscar_pelo_nome_do_setor, lambda{|nome| where(:nome => nome)}

private
	def transformar_nome_em_caixa_alta
  		self.nome.mb_chars.upcase.to_s if self.nome
	end
end