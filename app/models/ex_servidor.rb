#-*- coding:utf-8 -*-
class ExServidor < ActiveRecord::Base
	has_paper_trail

	validate :nova_matricula, presence: false, allow_blank: true, uniqueness: false
	validate :matricula_antiga, presence: false, allow_blank: true, uniqueness: false
	validate :nome, uniqueness: true, presence: false, allow_blank: true
	validate :cpf, allow_blank: true, uniqueness: false, presence: false
	
	validate :data_de_desligamento, allow_blank: true, uniqueness: false, presence: false
	
	validate :vinculo, allow_blank: true, uniqueness: false, presence: true
	validate :nome_funcao, allow_blank: true, uniqueness: false, presence: false

	# ....
		validate :transformar_campos_em_caixa_alta
	# ....
	
	def esta_em_setor
		order_cargas.pendentes
	end

	def pendente?
		order_cargas.where(status: false).any?
	end

	has_many :special_line_items
	has_many :order_cargas, through: :special_line_items

	belongs_to 	:setor_atual,
				:class_name => "Setor",
				:foreign_key => "setor_atual_id"

	def setor
		setor_atual.nome
	end

	def name; nome end

private
	def transformar_campos_em_caixa_alta
		self.nome.mb_chars.upcase.to_s if self.nome
		self.vinculo.mb_chars.upcase.to_s if self.vinculo
		self.nome_funcao.mb_chars.upcase.to_s if self.nome_funcao
	end

	def self.localizar_por_nova_matricula(search)
  		if search
    		find_by('nova_matricula LIKE ?', "%#{search}%")
  		else
    		scoped
  		end
	end

	def self.search_by_name(search_string)
  		if search_string
    		find_by('nome LIKE ?', "%#{search_string}%")
  		else
    		scoped
  		end
	end
end