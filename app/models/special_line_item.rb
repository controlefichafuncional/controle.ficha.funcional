class SpecialLineItem < ActiveRecord::Base
   	belongs_to :ex_servidor,  class_name: "ExServidor",
                      		  foreign_key: "ex_servidor_id"

  	belongs_to :special_cart,	class_name: "Cart",
                    		foreign_key: "special_cart_id"
                    
  	belongs_to :order_carga, 	class_name: "OrderCarga",
                      		foreign_key: "order_carga_id",
                      		dependent: :delete
end
