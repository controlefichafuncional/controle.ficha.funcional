#ENV['BUNDLE_GEMFILE'] = File.expand_path('../Gemfile', File.dirname(__FILE__))
#require 'bundler/setup'

# Set the working application directory
# working_directory "/path/to/your/app"
working_directory "/var/www/sisconff"

# Unicorn PID file location
# pid "/path/to/pids/unicorn.pid"

# Path to logs
# stderr_path "/path/to/log/unicorn.log"
# stdout_path "/path/to/log/unicorn.log"
stderr_path "/tmp/unicorn-error.log"
stdout_path "/tmp/unicorn-out.log"

# Unicorn socket
# listen "/tmp/unicorn.[app name].sock"
listen "/tmp/unicorn.sisconff.sock"
pid "/tmp/unicorn.pid"

# Number of processes
# worker_processes 4
worker_processes 2

# Time-out
timeout 30
preload_app true
