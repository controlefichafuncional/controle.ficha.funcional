#encoding:utf-8
ControleFichaFuncional::Application.routes.draw do
  scope '(:locale)' do
    resources :control, :only =>[:index, :get_order_carga],:collection=> { :receive_all => :put } 
    
    resources :usuarios, :only =>[:index, :edit, :update_password]
    
    resources :fichas, :only => [:index, :new, :edit, :create, :update, :destroy]
    
    resources :pensionistas
  
    resources :ex_servidores
    
    resources :line_items
    
    resources :special_line_items
    
    resources :carts
    
    resources :special_carts
    
    resources :order_cargas,:only => [:index, :new, :edit, :create, :destroy]

    resources :special_order_cargas,:only => [:new, :create, :destroy]
    
    resources :search, :only =>[:track, :track_by_sector, :locate_string, :locate_fichas_by_sector]
    
    resources :setores
    # resources :ficha_steps


    devise_for :usuarios
    
    scope "/usuario" do
      get  "/minhas-configurações/edit" => "usuarios#edit", as: :keystroke
      put "/minhas-configurações/update" => "usuarios#update_password", as: :keystroke_update_password
    end

    authenticated :usuario do
      root to: "welcome#sisconff_screen"
    end

    as :order_cargas do
      get "order_cargas/:id" => "order_cargas#show",   as: :order_cargas_id
      get "etiqueta"=> "order_cargas#index", :as=>:view_cargas
      get "/espelho/:id" => "pdf_reports#imprimir_carga", :format => "pdf", :as=> :mirror
    end

    scope '/ex-servidores' do
      as :special_order_cargas do
        get "special_order_cargas/new"=> "special_order_cargas#new",   as: :new_exservidor_cargas
        
        get "special_order_cargas/:id" => "special_order_cargas#show",   as: :exservidor_cargas_id
        # get "exservidor-etiqueta"=> "special_order_cargas#index", :as=>:view_cargas
        get "/exservidor-espelho/:id" => "pdf_reports#imprimir_exservidor_carga", :format => "pdf", :as=> :exservidor_mirror
      
        post "/special_order_cargas/create" => "special_order_cargas#create", as: :special_order_carga_create
        get '/special_order_cargas/ex_servidores/:id', to: 'special_order_cargas#show', as: :special_show_carga
      end
    end

    scope "control" do
      get "/gerenciar/cargas", to: 'control#index', as: :control_set_new_carga
      get "/gerenciar/cargas/ex_servidores", to: 'control#new_ex_serv_frm', as: :control_new_ex_serv_cargas
    end

    scope "visualizar" do
      get '/cargas-pendentes', to: 'control#get_order_carga', as: :control_get_order_carga
      put '/receber-cargas-pendentes', to: 'control#receive_all', as: :control_receive_all
      get '/carga/visualizar-matriculas/:carga_id', to: 'control#view_matriculas', as: :control_view_matriculas
      get '/carga/visualizar-exservidor-matriculas/:special_carga_id', to: 'control#view_exservidor_matriculas', as: :control_view_exservidor_matriculas
    end

    as :search do
      get "search/localizar_ficha_por_matricula/",            to: 'search#track',as: :find_matricula
      get "search/localizar_servidor_por_nome/",            to: 'search#track_by_name',as: :find_server_name
      get "search/ficha/:ficha_id",         to: 'search#locate_ficha',as: :find_ficha_omega
      get "search/exservidor/:exservidor_id",      to: 'search#locate_exservidor',as: :find_exservidor_omega
      get "search/setor/ficha/:ficha_id/exservidor/:exservidor_id",  to: 'search#locate_both',as: :find_paradise_city

      get "search/",  to: 'search#locate_both',as: :find_nome

      get "search/localizar_setores/",            to: 'search#track_by_sector',as: :find_setores  
      get "search/setor/:id",               to: 'search#locate_fichas_by_sector',as: :sectors_located
      
      get "search/localizar_ficha",      to: 'search#localizar', as: :search_string
      get "search/localizar_servidor",      to: 'search#localizar_por_nome', as: :search_server
    end

    as :fichas do
      get "fichas/index"    => "fichas#index",  as: :_fichas
      get "fichas/view/:id" => "fichas#show",   as: :_show_ficha
      get "fichas/new"      => "fichas#new",    as: :_new_ficha
      get "fichas/edit/:id" => "fichas#edit",   as: :_edit_ficha
      get "fichas/create"   => "fichas#create", as: :_create_ficha
      get "fichas/update"   => "fichas#update", as: :_update_ficha
    end

    scope :sisconff do
      get '/sistema-de-controle-de-ficha-funcional', to: "welcome#sisconff_screen", as: :sisconff
      mount RailsAdmin::Engine => '/admin', :as => 'rails_admin'
    end

    scope "/welcome" do
      get "/help", to: 'welcome#help', as: :help
    end
    
    root to: "welcome#index", as: 'welcome', via: :all
  end  
end
