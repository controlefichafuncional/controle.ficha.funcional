$:.unshift './lib'
require 'mina/bundler'
require 'mina/rails'
require 'mina/git'

# require 'mina/defaults'
# require 'mina/extras'
# require 'mina/unicorn'
# require 'mina/nginx'

Dir['lib/mina/servers/*.rb'].each { |f| load f }

###########################################################################
# Common settings for all servers
###########################################################################

set :domain, '10.0.0.4'
set :user, 'deployer'
set :deploy_to, '/var/www/sisconff'
set :port, 60120

set :app, 'sisconff'
set :keep_releases, 10

set :repository, 'git@bitbucket.org:controlefichafuncional/controle.ficha.funcional.git'
set :branch, 'master'

set :shared_paths, ['config/database.yml', 'log']

task :setup => :environment do
  queue! %[mkdir -p "#{deploy_to}/shared/log"]
  queue! %[chmod g+rx,u+rwx "#{deploy_to}/shared/log"]

  queue! %[mkdir -p "#{deploy_to}/shared/config"]
  queue! %[chmod g+rx,u+rwx "#{deploy_to}/shared/config"]

  queue! %[touch "#{deploy_to}/shared/config/database.yml"]
  queue  %[echo "-----> Be sure to edit 'shared/config/database.yml'."]
end

###########################################################################
# Tasks
###########################################################################

# set :server, ENV['to'] || default_server
# invoke :"env:#{server}"

desc "Deploys the current version to the server."
task :deploy do
  deploy do
    invoke :'git:clone'
    invoke :'deploy:link_shared_paths'
    invoke :'bundle:install'
    invoke :'rails:db_migrate'
    invoke :'rails:db_seed'
    invoke :'rails:assets_precompile'

    to :launch do
      invoke :'unicorn:restart'
    end
  end
end
