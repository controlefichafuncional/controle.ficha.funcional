RailsAdmin.config do |config|

  config.main_app_name = ["SISCONFF", "Painel do Administrador"]

  ### Popular gems integration

  ## == Devise ==
  config.authenticate_with do
    warden.authenticate! scope: :usuario
  end
  config.current_user_method(&:current_usuario)


  ## == Cancan ==
  config.authorize_with :cancan

  ## == PaperTrail ==
  config.audit_with :paper_trail, 'Usuario', 'PaperTrail::Version' # PaperTrail >= 3.0.0

  ### More at https://github.com/sferik/rails_admin/wiki/Base-configuration

  config.actions do
    dashboard                     # mandatory
    index                         # mandatory
    new
    export
    bulk_delete
    show
    edit
    delete
    show_in_app

    ## With an audit adapter, you can add:
    # history_index
    # history_show
  end

  config.model Usuario do
    edit do
      field :nome
      
      field :cpf do
        label "CPF - \n (000.000.000-00)"
      end
      
      field :matricula
      field :email
      field :password
      
      field :password_confirmation do
        label "Confirme a senha"
      end
      
      field :setor
      field :admin
    end
  end

  config.model ExServidor do
    edit do
      field :nova_matricula
      field :matricula_antiga
      field :nome
      
      field :cpf do
        label "CPF - \n (000.000.000-00)"
      end
      
      field :data_de_admissao
      field :data_de_desligamento
      field :data_de_aposentadoria
      
      field :vinculo
      field :nome_funcao
      field :setor_atual
    end
  end

  config.model Setor do
    edit do
      field :nome
    end
  end

  config.current_user_method { current_usuario }

end
