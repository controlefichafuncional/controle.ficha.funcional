source 'https://rubygems.org'

gem 'rails', '4.0.4'
gem 'devise'
gem 'paperclip'
gem 'will_paginate'
gem "cancan"
gem 'bcrypt-ruby', '~> 3.1.2'
gem "activerecord-import"
gem 'twitter-bootstrap-rails', :git=>'git://github.com/seyhunak/twitter-bootstrap-rails.git'

# => START::PDF:INSTALATION
gem 'prawn'
gem 'wkhtmltopdf-binary' # => Use these comands to install: sudo -s and apt-get install wkhtmltopdf
# => END::PDF:INSTALATION

group :development, :test do
	gem 'guard'
	gem 'pry'
	gem 'pry-nav'
	gem 'pry-rails'
end

gem 'mina'
gem 'kramdown', :groups => [:development], :require => false
gem 'psych'

# START: RAILS_ADMIN
gem 'rails_admin', :git => 'git://github.com/sferik/rails_admin.git'
# END: RAILS_ADMIN

# START: (PLUGINS::DATABASE)
gem 'mysql2'
gem 'sqlite3',  group: :test
gem 'paper_trail', github: 'airblade/paper_trail'
# END: (PLUGINS::DATABASE)

# START: (PLUGINS::ASSETS)
group :assets do
	gem 'json'
	gem 'bson_ext'
	gem 'jquery-rails'
	gem 'jquery-ui-rails'
	gem 'sass-rails', '~> 4.0.0'
	gem 'uglifier', '>= 1.3.0'
	gem 'coffee-rails', '~> 4.0.0'
	gem 'therubyracer', platforms: :ruby
end
# END: (PLUGINS::ASSETS)

gem 'jbuilder', '~> 1.2'
gem 'activerecord-session_store', github: 'rails/activerecord-session_store'

group :doc do
  # USE rake doc:rails PARA GERAR A DOCUMENTAÇÃO DA API DO RAILS
  gem 'sdoc', require: false
end

# START: (PLUGINS::DEPLOY)
gem 'unicorn'
# END: (PLUGINS::DEPLOY)

#START:plugins
gem "slim-rails"
#END:plugins

# START: (PLUGINS::Test)
group :test do
	gem 'cucumber'
	gem 'capybara'
	gem 'rspec-rails'
	gem 'cucumber-rails', :require => false
		
	gem 'factory_girl_rails'
	gem 'email_spec'
	
	gem 'guard-rspec'
	gem 'database_cleaner', :git => 'git://github.com/bmabey/database_cleaner.git' # database_cleaner não é requerido, mas altamente recomendado
	gem "launchy"

	gem 'test_notifier'
	gem 'autotest-standalone'
	gem "rb-fsevent"

	gem "mocha"
	gem 'turn', '~> 0.8.3', :require => false

	gem "pdf-inspector", :require => "pdf/inspector", :git => "https://github.com/prawnpdf/pdf-inspector.git"
end
# END: (PLUGINS::Test)
