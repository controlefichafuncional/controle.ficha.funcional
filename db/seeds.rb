#-*- coding:utf-8 -*-

require 'csv'
require 'date'
require "benchmark"

# => INICIALIZANDO FICHAS

setor = []
pensionista = []
servidor = []
exservidor = []

# => ......................

puts "Importing SMAGP Sectors ..."

CSV.foreach(Rails.root.join("sectors.csv"), headers: true) do |row|
  setor << Setor.new(nome: row[0])
end

puts 'Importing SMAGP Main Users ...'
users = []
CSV.foreach(Rails.root.join("SISCONFF_USUARIOS.csv"), headers: true) do |row|
  Usuario.new({
    email: row[0],
    nome: row[1],
    matricula: row[2],
    cpf: row[3],
    admin: row[4],
    setor_id: row[5],
    password: row[6]
  }).save(:validate => false, :skip_callbacks => true)
end

puts 'Importing SMAGP Server Data ...'

CSV.foreach(Rails.root.join("servidores.csv"), headers: true) do |row|
  '''
    SE NÃO FOR VERDADEIRO INSERE EM FICHA MESMO
    CASO CONTRARIO INSERE EM PENSIONISTA OS CAMPOS
    matricula, nome, cpf, situacao, vinculo, data_admissao
  '''

  case row[9]
    when "true"

      _situacao = ""
      case row[7]
         when "1"
           _situacao = "ATIVO"
         when "2"
           _situacao = "AFASTADO"
         when "3"
           _situacao = "DESLIGADO"
      end

      pensionista << [
        row[0],
        row[2],
        row[1],
        row[6],
        _situacao,
        row[8]
      ]
    when "false"

      _situacao = ""
      case row[7]
         when "1"
           _situacao = "ATIVO"
         when "2"
           _situacao = "AFASTADO"
         when "3"
           _situacao = "DESLIGADO"
      end

      setor_atual_id = 1
      
      servidor << [
        row[0],
        row[1],
        row[2],
        row[3],
        row[4],
        row[5],
        false,
        row[6],
        _situacao,
        row[8],
        setor_atual_id
      ]
  end
end


# puts 'Importing SMAGP Ex-Server Data ...'
data_de_desligamento = ""
# => START::REVERSOR_DE_STRING
  def match_string_to_reverse_format(str)
    return nil if (str.nil? or str.size < 8)
    str.match /^(\d{1,2})\/(\d{1,2})\/(\d\d\d\d)/
    r = ("%s-%02d-%02d" % [str[3], str[1].to_i, str[2].to_i]).to_s
    r
  end
# # => END::REVERSOR_DE_STRING

def remove_zero_on_the_left(termo_to_remove)
  return termo_to_remove.gsub( /^0 {n} + /, '').to_i.to_s if termo_to_remove.is_a? String
end


CSV.foreach(Rails.root.join("exserv2.csv"), headers: true) do |row|
  '''
    ADICIONANDO EX-SERVIDORES
  '''
  setor_atual_id = 11

  unless row[3].nil?
    data_de_desligamento=row[3]
    data_de_desligamento=match_string_to_reverse_format(data_de_desligamento)    
  else
    data_de_desligamento = "0000-00-00"
  end

  exservidor << [
    row[0],
    remove_zero_on_the_left(row[1]),
    row[2],
    data_de_desligamento,
    # row[3],
    row[4],
    row[5],
    setor_atual_id,
    row[6]
  ]
end

Setor.import setor, validate: false

Pensionista.import [:matricula, :cpf, :nome, :nome_vinculo, :situacao, :data_admissao], pensionista, validate: false
Ficha.import [:matricula, :nome, :cpf, :nome_funcao, :nome_lotacao,
  :nome_local_trabalho, :pensionista, :nome_vinculo, :situacao, :data_admissao, :setor_atual_id], servidor, validate: false

ExServidor.import [:nome, :nova_matricula, :cpf, :data_de_desligamento, :vinculo, :nome_funcao, :setor_atual_id, :matricula_antiga], exservidor, validate: false; 0


puts '## ..................................................... ##'
puts "## ............. Dados Importados com Sucesso .......... ##"
puts '## ..................................................... ##'