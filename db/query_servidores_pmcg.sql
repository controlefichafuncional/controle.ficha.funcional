select
	rtrim(fix.MTCFNC) 		as 'matricula',
	rtrim(fix.NOMFNC) 		as 'nome',
	doc.numcpf 			as 'cpf',
	rtrim(fun.NOMFUN) 		as 'funcao',
	rtrim(ltc.NomLtc) 		as 'lotacao',
	rtrim(lct.NOMLCLTRB) 	as 'local_trablho',
	clf.DesClfFnc	 		as 'vinculo',
	rtrim(mns.SITFNC) 		as 'situacao',
	rtrim(fix.DATADSFNC)	as 'data_admissao',
	'false'				as 'pensionista'
from TbRhPssFix fix
left join TbRhPssMns mns
	on fix.MTCFNC = mns.MTCFNC
left join TbRhDoc doc
	on fix.MTCFNC = doc.MTCFNC
left join TBRhCrgFun fun
	on mns.CODFUN = fun.CODFUN
left join TbRhLtc ltc
	on mns.CodLtc = ltc.CodLtc
left join TbRhLclTrb lct
	on mns.CODLCLTRB = lct.CODLCLTRB
left join TbRhClfFnc clf
	on mns.ClfFnc = clf.ClfFnc
where
	(mns.MESREF = 201404 or mns.MESREF is null)
and
	mns.ClfFnc in (6, 10, 30, 67, 70, 82, 84, 85)
union
select
	pns.MTCFNC 		as 'matricula',
	rtrim(pns.NOMPNSVTC) 		as 'nome',
	rtrim(pns.NUMCPFPNSVTC) 	as 'cpf',
	'pensionista' 		as 'funcao',
	'pensionista' 		as 'lotacao',
	'pensionista' 		as 'local_trablho',
	'pensionista' 		as 'vinculo',
	case when DATFIMPNSVTC > getdate() or DATFIMPNSVTC is null
	then '1'
	else '3'
	end 				as 'vinculo',
	pns.DATINIPNSVTC 	as 'data_admissao',
	'true'			as 'pensionista'
from TbRhPnsVtc pns