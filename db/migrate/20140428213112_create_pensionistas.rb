class CreatePensionistas < ActiveRecord::Migration
  def change
    create_table :pensionistas do |t|
      t.string :matricula
      t.string :cpf
      t.string :nome
      t.string :situacao
      t.date :data_admissao

      t.timestamps
    end
  end
end
