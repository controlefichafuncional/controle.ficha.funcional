class AddSetorAtualToExServidores < ActiveRecord::Migration
  def change
    add_column :ex_servidores, :setor_atual_id, :integer
    add_index(:ex_servidores, [:setor_atual_id], :unique => false)
  end
end
