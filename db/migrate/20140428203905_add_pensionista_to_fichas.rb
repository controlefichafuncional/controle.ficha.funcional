class AddPensionistaToFichas < ActiveRecord::Migration
  def change
    add_column :fichas, :pensionista, :boolean
  end
end
