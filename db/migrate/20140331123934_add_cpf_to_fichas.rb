class AddCpfToFichas < ActiveRecord::Migration
  def change
    add_column :fichas, :cpf, :string, :default=> "Favor Preencher se tiver CPF"
    add_index(:fichas, [:cpf], :unique =>false)
  end
end
