#encoding:utf-8
class DeviseCreateUsuarios < ActiveRecord::Migration
  def change
    create_table(:usuarios) do |t|
      ## Campos de authenticação
      t.string :email,              :null => false, :default => ""
      t.string :encrypted_password, :null => false, :default => ""

      ## Modulo de recuperação
      t.string   :reset_password_token
      t.datetime :reset_password_sent_at

      ## Modulo de relembrar
      t.datetime :remember_created_at

      ## Trackable
      t.integer  :sign_in_count, :default => 0, :null => false
      t.datetime :current_sign_in_at
      t.datetime :last_sign_in_at
      t.string   :current_sign_in_ip
      t.string   :last_sign_in_ip

      ## Modulo de confirmação
      # t.string   :confirmation_token
      # t.datetime :confirmed_at
      # t.datetime :confirmation_sent_at
      # t.string   :unconfirmed_email # Only if using reconfirmable

      ## Modulo de bloqueio
      t.integer  :failed_attempts, :default => 0, :null => false # Only if lock strategy is :failed_attempts
      t.string   :unlock_token # Only if unlock strategy is :email or :both
      t.datetime :locked_at

      t.timestamps
    end

    add_index :usuarios, :email,                :unique => false
    #add_index :usuarios, :reset_password_token, :unique => true
    # add_index :usuarios, :confirmation_token,   :unique => true
    #add_index :usuarios, :unlock_token,         :unique => true
  end
end