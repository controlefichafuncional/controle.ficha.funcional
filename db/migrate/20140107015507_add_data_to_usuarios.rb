#encoding:utf-8
class AddDataToUsuarios < ActiveRecord::Migration
  def change
    add_column :usuarios, :nome, :string
    add_column :usuarios, :matricula, :string
    add_index :usuarios, :nome, :unique => true
  end
end
