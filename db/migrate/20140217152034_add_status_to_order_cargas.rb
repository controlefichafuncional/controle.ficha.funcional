class AddStatusToOrderCargas < ActiveRecord::Migration
  def change
    add_column :order_cargas, :status, :boolean, default: 0
  end
end
