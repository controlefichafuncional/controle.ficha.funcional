class AddSetorAtualToFichas < ActiveRecord::Migration
  def change
    add_column :fichas, :setor_atual_id, :integer
    add_index(:fichas, [:setor_atual_id], :unique => false)
  end
end
