class CreateExServidores < ActiveRecord::Migration
  def change
    create_table :ex_servidores do |t|
      t.string :nome
      t.string :nova_matricula
      t.string :cpf
      t.string :data_de_desligamento
      t.string :vinculo
      t.string :nome_funcao
      t.string :matricula_antiga

      t.timestamps
    end
    add_index(:ex_servidores, [:nome], type: :fulltext ,unique: false)
    add_index(:ex_servidores, [:nova_matricula], :unique => false)
  end
end
