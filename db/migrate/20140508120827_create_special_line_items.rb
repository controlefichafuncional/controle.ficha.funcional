class CreateSpecialLineItems < ActiveRecord::Migration
  def change
    create_table :special_line_items do |t|
      t.references :special_cart, index: true
      t.references :ex_servidor, index: true
      t.references :order_carga, index: true

      t.timestamps
    end
  end
end
