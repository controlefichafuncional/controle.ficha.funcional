# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140516151545) do

  create_table "carts", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "ex_servidores", force: true do |t|
    t.string   "nome"
    t.string   "nova_matricula"
    t.string   "cpf"
    t.string   "data_de_desligamento"
    t.string   "vinculo"
    t.string   "nome_funcao"
    t.string   "matricula_antiga"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "setor_atual_id"
  end

  add_index "ex_servidores", ["nome"], name: "index_ex_servidores_on_nome", type: :fulltext
  add_index "ex_servidores", ["nova_matricula"], name: "index_ex_servidores_on_nova_matricula", using: :btree
  add_index "ex_servidores", ["setor_atual_id"], name: "index_ex_servidores_on_setor_atual_id", using: :btree

  create_table "fichas", force: true do |t|
    t.string   "matricula"
    t.string   "nome"
    t.string   "nome_funcao"
    t.string   "nome_lotacao"
    t.string   "nome_vinculo"
    t.string   "situacao"
    t.date     "data_admissao"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "cpf",                 default: "Favor Preencher se tiver CPF"
    t.string   "nome_local_trabalho"
    t.boolean  "pensionista"
    t.integer  "setor_atual_id"
  end

  add_index "fichas", ["cpf"], name: "index_fichas_on_cpf", using: :btree
  add_index "fichas", ["matricula"], name: "index_fichas_on_matricula", using: :btree
  add_index "fichas", ["nome"], name: "index_fichas_on_nome", type: :fulltext
  add_index "fichas", ["setor_atual_id"], name: "index_fichas_on_setor_atual_id", using: :btree

  create_table "fichas_order_cargas", force: true do |t|
    t.integer  "ficha_id"
    t.integer  "order_carga_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "fichas_order_cargas", ["ficha_id"], name: "index_fichas_order_cargas_on_ficha_id", using: :btree
  add_index "fichas_order_cargas", ["order_carga_id"], name: "index_fichas_order_cargas_on_order_carga_id", using: :btree

  create_table "line_items", force: true do |t|
    t.integer  "ficha_id"
    t.integer  "cart_id"
    t.integer  "order_carga_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "line_items", ["cart_id"], name: "index_line_items_on_cart_id", using: :btree
  add_index "line_items", ["ficha_id"], name: "index_line_items_on_ficha_id", using: :btree
  add_index "line_items", ["order_carga_id"], name: "index_line_items_on_order_carga_id", using: :btree

  create_table "order_cargas", force: true do |t|
    t.integer  "gerada_pelo_usuario_id"
    t.integer  "recebida_pelo_usuario_id"
    t.integer  "setor_de_origem_id"
    t.integer  "setor_de_destino_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "status",                   default: false
  end

  create_table "pensionistas", force: true do |t|
    t.string   "matricula"
    t.string   "cpf"
    t.string   "nome"
    t.string   "situacao"
    t.date     "data_admissao"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "nome_vinculo"
  end

  create_table "setores", force: true do |t|
    t.string   "nome"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "special_carts", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "special_line_items", force: true do |t|
    t.integer  "special_cart_id"
    t.integer  "ex_servidor_id"
    t.integer  "order_carga_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "special_line_items", ["ex_servidor_id"], name: "index_special_line_items_on_ex_servidor_id", using: :btree
  add_index "special_line_items", ["order_carga_id"], name: "index_special_line_items_on_order_carga_id", using: :btree
  add_index "special_line_items", ["special_cart_id"], name: "index_special_line_items_on_special_cart_id", using: :btree

  create_table "usuarios", force: true do |t|
    t.string   "email",                  default: ""
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.integer  "failed_attempts",        default: 0,     null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "nome"
    t.string   "matricula"
    t.integer  "setor_id"
    t.string   "cpf"
    t.boolean  "admin",                  default: false, null: false
  end

  add_index "usuarios", ["email"], name: "index_usuarios_on_email", using: :btree
  add_index "usuarios", ["nome"], name: "index_usuarios_on_nome", unique: true, using: :btree

  create_table "versions", force: true do |t|
    t.string   "item_type",  null: false
    t.integer  "item_id",    null: false
    t.string   "event",      null: false
    t.string   "whodunnit"
    t.text     "object"
    t.datetime "created_at"
  end

  add_index "versions", ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id", using: :btree

end
