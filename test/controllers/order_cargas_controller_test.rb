require 'test_helper'

class OrderCargasControllerTest < ActionController::TestCase
  setup do
    @order_carga = order_cargas(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:order_cargas)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create order_carga" do
    assert_difference('OrderCarga.count') do
      post :create, order_carga: { gerada_pelo_usuario_id: @order_carga.gerada_pelo_usuario_id, recebida_pelo_usuario_id: @order_carga.recebida_pelo_usuario_id, setor_de_destino_id: @order_carga.setor_de_destino_id, setor_de_origem_id: @order_carga.setor_de_origem_id }
    end

    assert_redirected_to order_carga_path(assigns(:order_carga))
  end

  test "should show order_carga" do
    get :show, id: @order_carga
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @order_carga
    assert_response :success
  end

  test "should update order_carga" do
    patch :update, id: @order_carga, order_carga: { gerada_pelo_usuario_id: @order_carga.gerada_pelo_usuario_id, recebida_pelo_usuario_id: @order_carga.recebida_pelo_usuario_id, setor_de_destino_id: @order_carga.setor_de_destino_id, setor_de_origem_id: @order_carga.setor_de_origem_id }
    assert_redirected_to order_carga_path(assigns(:order_carga))
  end

  test "should destroy order_carga" do
    assert_difference('OrderCarga.count', -1) do
      delete :destroy, id: @order_carga
    end

    assert_redirected_to order_cargas_path
  end
end
