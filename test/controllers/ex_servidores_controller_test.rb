require 'test_helper'

class ExServidoresControllerTest < ActionController::TestCase
  setup do
    @ex_servidor = ex_servidores(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:ex_servidores)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create ex_servidor" do
    assert_difference('ExServidor.count') do
      post :create, ex_servidor: { cpf: @ex_servidor.cpf, data_de_admissao: @ex_servidor.data_de_admissao, data_de_aposentadoria: @ex_servidor.data_de_aposentadoria, data_de_desligamento: @ex_servidor.data_de_desligamento, matricula_antiga: @ex_servidor.matricula_antiga, nome: @ex_servidor.nome, nome_função: @ex_servidor.nome_função, nova_matricula: @ex_servidor.nova_matricula, vinculo: @ex_servidor.vinculo }
    end

    assert_redirected_to ex_servidor_path(assigns(:ex_servidor))
  end

  test "should show ex_servidor" do
    get :show, id: @ex_servidor
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @ex_servidor
    assert_response :success
  end

  test "should update ex_servidor" do
    patch :update, id: @ex_servidor, ex_servidor: { cpf: @ex_servidor.cpf, data_de_admissao: @ex_servidor.data_de_admissao, data_de_aposentadoria: @ex_servidor.data_de_aposentadoria, data_de_desligamento: @ex_servidor.data_de_desligamento, matricula_antiga: @ex_servidor.matricula_antiga, nome: @ex_servidor.nome, nome_função: @ex_servidor.nome_função, nova_matricula: @ex_servidor.nova_matricula, vinculo: @ex_servidor.vinculo }
    assert_redirected_to ex_servidor_path(assigns(:ex_servidor))
  end

  test "should destroy ex_servidor" do
    assert_difference('ExServidor.count', -1) do
      delete :destroy, id: @ex_servidor
    end

    assert_redirected_to ex_servidores_path
  end
end
