require 'test_helper'

class SpecialLineItemsControllerTest < ActionController::TestCase
  setup do
    @special_line_item = special_line_items(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:special_line_items)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create special_line_item" do
    assert_difference('SpecialLineItem.count') do
      post :create, special_line_item: { exservidor_id: @special_line_item.exservidor_id, order_carga_id: @special_line_item.order_carga_id, special_cart_id: @special_line_item.special_cart_id }
    end

    assert_redirected_to special_line_item_path(assigns(:special_line_item))
  end

  test "should show special_line_item" do
    get :show, id: @special_line_item
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @special_line_item
    assert_response :success
  end

  test "should update special_line_item" do
    patch :update, id: @special_line_item, special_line_item: { exservidor_id: @special_line_item.exservidor_id, order_carga_id: @special_line_item.order_carga_id, special_cart_id: @special_line_item.special_cart_id }
    assert_redirected_to special_line_item_path(assigns(:special_line_item))
  end

  test "should destroy special_line_item" do
    assert_difference('SpecialLineItem.count', -1) do
      delete :destroy, id: @special_line_item
    end

    assert_redirected_to special_line_items_path
  end
end
