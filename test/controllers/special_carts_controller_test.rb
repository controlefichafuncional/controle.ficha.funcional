require 'test_helper'

class SpecialCartsControllerTest < ActionController::TestCase
  setup do
    @special_cart = special_carts(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:special_carts)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create special_cart" do
    assert_difference('SpecialCart.count') do
      post :create, special_cart: {  }
    end

    assert_redirected_to special_cart_path(assigns(:special_cart))
  end

  test "should show special_cart" do
    get :show, id: @special_cart
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @special_cart
    assert_response :success
  end

  test "should update special_cart" do
    patch :update, id: @special_cart, special_cart: {  }
    assert_redirected_to special_cart_path(assigns(:special_cart))
  end

  test "should destroy special_cart" do
    assert_difference('SpecialCart.count', -1) do
      delete :destroy, id: @special_cart
    end

    assert_redirected_to special_carts_path
  end
end
