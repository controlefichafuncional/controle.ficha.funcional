require 'test_helper'

class PensionistasControllerTest < ActionController::TestCase
  setup do
    @pensionista = pensionistas(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:pensionistas)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create pensionista" do
    assert_difference('Pensionista.count') do
      post :create, pensionista: { cpf: @pensionista.cpf, data_admissao: @pensionista.data_admissao, matricula: @pensionista.matricula, nome: @pensionista.nome, situacao: @pensionista.situacao }
    end

    assert_redirected_to pensionista_path(assigns(:pensionista))
  end

  test "should show pensionista" do
    get :show, id: @pensionista
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @pensionista
    assert_response :success
  end

  test "should update pensionista" do
    patch :update, id: @pensionista, pensionista: { cpf: @pensionista.cpf, data_admissao: @pensionista.data_admissao, matricula: @pensionista.matricula, nome: @pensionista.nome, situacao: @pensionista.situacao }
    assert_redirected_to pensionista_path(assigns(:pensionista))
  end

  test "should destroy pensionista" do
    assert_difference('Pensionista.count', -1) do
      delete :destroy, id: @pensionista
    end

    assert_redirected_to pensionistas_path
  end
end
