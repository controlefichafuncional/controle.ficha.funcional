require 'test_helper'

class SpecialOrderCargasControllerTest < ActionController::TestCase
  test "should get new" do
    get :new
    assert_response :success
  end

  test "should get _form" do
    get :_form
    assert_response :success
  end

  test "should get destroy" do
    get :destroy
    assert_response :success
  end

end
