#-*- coding:utf-8 -*-

require 'csv'
require 'pry'

exservidor = {}

require File.expand_path('../../config/environment', __FILE__) unless defined?(Rails)

puts 'ATUALIZANDO DADOS DOS SERVIDORES E EXSERVIDORES'

# => START::SITUACAO
def situacao(argumento)
  _situacao =""
  case argumento
      when "1"
          _situacao = "ATIVO"
      when "2"
          _situacao = "AFASTADO"
      when "3"
          _situacao = "DESLIGADO"
    end 
end
# => END::SITUACAO

# => START::DATA_DE_DESLIGAMENTO
def data_desligamento(argumento)
  unless argumento.nil?
    data_de_desligamento=argumento
    data_de_desligamento=match_string_to_reverse_format(data_de_desligamento)    
  else
    data_de_desligamento = "0000-00-00"
  end
end
# => END::DATA_DE_DESLIGAMENTO

# => START::REMOVE_ZERO_ON_THE_LEFT
  def remove_zero_on_the_left(termo_to_remove)
    return termo_to_remove.gsub( /^0 {n} + /, '').to_i.to_s if termo_to_remove.is_a? String
  end
# => END::REMOVE_ZERO_ON_THE_LEFT

# => START::REVERSOR_DE_STRING
  def match_string_to_reverse_format(str)
    return nil if (str.nil? or str.size < 8)
    str.match /^(\d{1,2})\/(\d{1,2})\/(\d\d\d\d)/
    r = ("%s-%02d-%02d" % [str[3], str[1].to_i, str[2].to_i]).to_s
    r
  end
# # => END::REVERSOR_DE_STRING

CSV.foreach(Rails.root.join("atualizacao_da_base/exservidores.csv"), headers: true) do |row|
  setor_atual_id = 11

  exservidor[row[1]] = [
    row[0],
    remove_zero_on_the_left(row[1]),
    row[2],
    data_desligamento(row[3]),
    row[4],
    row[5],
    setor_atual_id,
    row[6]
  ]
end

exservidores_existentes = ExServidor.all.pluck(:nova_matricula)
exservidores_existentes.each { |k| exservidor.delete k }

# binding.pry
ExServidor.import [:nome, :nova_matricula, :cpf, :data_de_desligamento, :vinculo, :nome_funcao, :setor_atual_id, :matricula_antiga], exservidor.values, validate: false; 0

puts '## ..................................................... ##'
puts "## ............. Dados Atualizados com Sucesso ......... ##"
puts '## ..................................................... ##'