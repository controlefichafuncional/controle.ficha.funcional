#-*- coding:utf-8 -*-

require 'csv'
# require 'pry'

pensionista = {}
servidor = {}
exservidor = {}

require File.expand_path('../../config/environment', __FILE__) unless defined?(Rails)

def situacao(argumento)
	_situacao =""
	case argumento
     	when "1"
       		_situacao = "ATIVO"
     	when "2"
       		_situacao = "AFASTADO"
     	when "3"
       		_situacao = "DESLIGADO"
    end	
end

CSV.foreach(Rails.root.join("atualizacao_da_base/servidores.csv"), headers: true) do |row|
  case row[9]
    when "true"
		pensionista[row[0]] = [
        	row[0],
        	row[2],
        	row[1],
        	row[6],
        	situacao(row[7]),
        	row[8]
      	]
    when "false"
		setor_atual_id = 2 # (1 => arquivo, 2 => cadastro)
      	servidor[row[0]] = [
        	row[0],
        	row[1],
        	row[2],
        	row[3],
        	row[4],
        	row[5],
        	false,
        	row[6],
        	situacao(row[7]),
        	row[8],
        	setor_atual_id
      	]
  	end
end

fichas_existentes = Ficha.all.pluck(:matricula)
pensionistas_existentes = Pensionista.all.pluck(:matricula)

fichas_existentes.each { |k| servidor.delete k }
pensionistas_existentes.each { |k| pensionista.delete k }

#binding.pry

puts 'ATUALIZANDO DADOS DOS SERVIDORES E EXSERVIDORES'

Ficha.import [:matricula, :nome, :cpf, :nome_funcao, :nome_lotacao,
  :nome_local_trabalho, :pensionista, :nome_vinculo, :situacao, :data_admissao, :setor_atual_id], servidor.values, validate: false
Pensionista.import [:matricula, :cpf, :nome, :nome_vinculo, :situacao, :data_admissao], pensionista.values, validate: false

puts "## #{servidor.count} servidores atualizados!             ##"
puts "## #{pensionista.count} pensionistas atualizados!        ##"
puts '## ..................................................... ##'
puts "## ............. Dados Atualizados com Sucesso ......... ##"
puts '## ..................................................... ##'
